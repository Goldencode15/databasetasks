package gs;

public class CRUD_cassandra implements Iconnect,Icrud{
    @Override
    public void connection() {
        System.out.println("Conection in progress.");
    }

    @Override
    public void status() {
        System.out.println("Connected Successfully");
    }

    @Override
    public void disconnect() {
        System.out.println("Disconnected");
    }

    @Override
    public void create() {
        System.out.println("Create");
    }

    @Override
    public void read() {
        System.out.println("Read");
    }

    @Override
    public void update() {
        System.out.println("Update");
    }

    @Override
    public void delete() {
        System.out.println("Delete");
    }
}
