package gs;


public class Db_utils {
    private String dbUrl, username, password, query;
    private DbType DbType;

    public Db_utils(String dbUrl, String username, String password, String query, gs.DbType dbType) {
        this.dbUrl = dbUrl;
        this.username = username;
        this.password = password;
        this.query = query;
        DbType = dbType;
    }


    public void factory() {
        switch (DbType) {
            case MONGODB:
                CRUD_mongo crud_mongo = new CRUD_mongo();
                crud_mongo.connection();
                crud_mongo.create();
                crud_mongo.delete();
                crud_mongo.disconnect();
                crud_mongo.read();
                crud_mongo.status();
                crud_mongo.update();
                break;
            case CASSANDRA:
                CRUD_cassandra crud_cassandra = new CRUD_cassandra();
                crud_cassandra.connection();
                crud_cassandra.create();
                crud_cassandra.read();
                crud_cassandra.update();
                crud_cassandra.delete();
                crud_cassandra.status();
                crud_cassandra.disconnect();
                break;
            case RDBMS:
                CRUD_rdbms crud_rdbms = new CRUD_rdbms();
                crud_rdbms.connection();
                crud_rdbms.status();
                crud_rdbms.create();
                crud_rdbms.read();
                crud_rdbms.update();
                crud_rdbms.delete();
                crud_rdbms.disconnect();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + DbType);
        }
    }
}
